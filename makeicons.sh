#!/bin/bash

defs="48,mdpi 72,hdpi 96,xhdpi 144,xxhdpi 192,xxxhdpi"

for def in $defs; do
	size=$(echo $def | awk -F, '{print $1}')
	folder=$(echo $def | awk -F, '{print $2}')
	echo $def $size $folder

	inkscape -w $size -h $size icon.svg -o android/app/src/main/res/mipmap-$folder/ic_launcher.png
done
