import 'package:flindex/flindex_data.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FlindexEdit extends StatefulWidget {
  const FlindexEdit({Key? key, required this.code}) : super(key: key);

  final String code;

  @override
  State<FlindexEdit> createState() => _FlindexEditState();
}

class _FlindexEditState extends State<FlindexEdit> {
  _FlindexEditState();

  final _editController = TextEditingController();
  final _addItemController = TextEditingController();

  void _save(FlindexData data) {
    data.setName(widget.code, _editController.text);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Name saved'),
    ));
  }

  void _addItem(FlindexData data, String item) {
    if (item.isEmpty) return;
    if (data.addItem(widget.code, item)) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Item added'),
      ));
      _addItemController.text = '';
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(
              Icons.warning,
              color: Colors.yellow,
            ),
            SizedBox(width: 8),
            Text('Item already in inventory'),
          ],
        ),
      ));
    }
  }

  _confirmDelete(FlindexData data) {
    showDialog<void>(
      context: context,
      builder: (ctx) {
        return AlertDialog(
          title: const Text('Delete inventory'),
          content: const Text(
              'Are you sure you want to delete the inventory and all items?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Text("No"),
            ),
            TextButton(
                onPressed: () {
                  data.removeCode(widget.code);
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/', ModalRoute.withName("/"));
                },
                child: const Text("Yes")),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FlindexData>(builder: (context, data, child) {
      _editController.text = data.getName(widget.code);
      return Scaffold(
        appBar: AppBar(
          title: Text('Edit: ${data.prettyName(widget.code)}'),
          actions: [
            IconButton(
                onPressed: () => _confirmDelete(data),
                icon: const Icon(Icons.delete)),
          ],
        ),
        body: Column(children: [
          TextField(
            controller: _editController,
            decoration: InputDecoration(
              labelText: 'Enter a memorable name for the code ${widget.code}',
              hintText: 'Name',
              suffixIcon: IconButton(
                  onPressed: () => _save(data), icon: const Icon(Icons.save)),
            ),
            onSubmitted: (value) => _save(data),
          ),
          Expanded(
              child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: data.count(widget.code),
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: const EdgeInsets.all(1),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(2.0)),
                          color: Theme.of(context).backgroundColor,
                        ),
                        height: 40,
                        child: Row(children: [
                          const SizedBox(width: 8),
                          Text('${data.items(widget.code, index)}'),
                          const Spacer(),
                          IconButton(
                              tooltip: 'Remove item',
                              onPressed: () => data.removeItem(
                                  widget.code, data.items(widget.code, index)),
                              icon: const Icon(Icons.delete))
                        ]),
                      ),
                    );
                  })),
          TextField(
            onSubmitted: (value) => _addItem(data, value),
            controller: _addItemController,
            decoration: InputDecoration(
              labelText: 'Add an item for ${data.prettyName(widget.code)}',
              hintText: 'Item',
            ),
          ),
        ]),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _addItem(data, _addItemController.text),
          tooltip: 'Add',
          child: const Icon(Icons.add),
        ),
      );
    });
  }
}
