import 'package:flindex/screens/display.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../flindex_data.dart';

class FlindexInventory extends StatelessWidget {
  const FlindexInventory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<FlindexData>(builder: (context, data, child) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Inventory list'),
        ),
        body: Center(
          child: ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: data.codeCount(),
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.all(4),
                  child: Ink(
                    decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.all(Radius.circular(2.0)),
                      color: Theme.of(context).backgroundColor,
                    ),
                    height: 50,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            settings: const RouteSettings(name: '/display'),
                            builder: (context) =>
                                FlindexDisplay(code: data.getCode(index)!),
                          ),
                        );
                      },
                      child: Row(children: [
                        const SizedBox(width: 8),
                        Text(data.prettyName(data.getCode(index))),
                        const Spacer(),
                      ]),
                    ),
                  ),
                );
              }),
        ),
      );
    });
  }
}
