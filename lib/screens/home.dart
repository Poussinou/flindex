import 'dart:developer';

import 'package:flindex/screens/code_scanner.dart';
import 'package:flindex/screens/edit.dart';
import 'package:flindex/screens/inventory.dart';
import 'package:flindex/screens/search.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:provider/provider.dart';
import 'display.dart';
import 'package:flindex/flindex_data.dart';

class FlindexHome extends StatefulWidget {
  const FlindexHome({Key? key}) : super(key: key);

  @override
  State<FlindexHome> createState() => _FlindexHomeState();
}

class _FlindexHomeState extends State<FlindexHome> {
  final _codeInputController = TextEditingController();
  bool nfcAvailable = false;

  void _handleScannedCode(String code) {
    var data = Provider.of<FlindexData>(context, listen: false);
    if (data.contains(code)) {
      _openCode(data, code);
    } else {
      _codeInputController.text = code;
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Unknown code: $code'),
      ));
    }
  }

  void _startNfc() async {
    var available = await NfcManager.instance.isAvailable();
    if (available) {
      // Start NFC Session
      NfcManager.instance.startSession(
        onDiscovered: (NfcTag tag) async {
          if (kDebugMode) {
            print('Tag discovered: ${tag.data}');
          }

          if (tag.data['nfca'].containsKey('identifier')) {
            String tagID = '';
            for (var part in tag.data['nfca']['identifier']) {
              tagID += part.toString().padLeft(3, '0');
            }
            if (kDebugMode) {
              print('Using tag id: $tagID');
            }
            var route = ModalRoute.of(context);
            if (route != null && route.isCurrent) {
              if (kDebugMode) {
                print('Currently on home screen, opening display page');
              }
              _handleScannedCode(tagID);
            }
          }
        },
      );

      setState(() {
        nfcAvailable = available;
      });
    }
  }

  @override
  void dispose() {
    // Stop NFC Session
    if (nfcAvailable) {
      NfcManager.instance.stopSession();
    }
    if (kDebugMode) {
      log("Home screen disposed, NFC session disabled");
    }
    super.dispose();
  }

  void _scanCode(FlindexData data) {
    Navigator.push(
      context,
      MaterialPageRoute(
          settings: const RouteSettings(name: '/scanner'),
          builder: (context) => const CodeScanner()),
    ).then((code) {
      if (code != "") {
        _handleScannedCode(code);
      }
    });
  }

  void _openCode(FlindexData data, String code) {
    if (code.isEmpty) return;
    if (data.contains(code)) {
      Navigator.push(
        context,
        MaterialPageRoute(
          settings: const RouteSettings(name: '/display'),
          builder: (context) => FlindexDisplay(code: code),
        ),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          settings: const RouteSettings(name: '/edit'),
          builder: (context) => FlindexEdit(code: code),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _startNfc();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FlindexData>(builder: (context, data, child) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Flindex inventory manager'),
          actions: [
            IconButton(
                tooltip: "Search in inventory",
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      settings: const RouteSettings(name: '/search'),
                      builder: (context) => FlindexSearch(),
                    ),
                  );
                },
                icon: const Icon(Icons.search)),
            IconButton(
                tooltip: "Open inventory overview",
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      settings: const RouteSettings(name: '/inventory'),
                      builder: (context) => const FlindexInventory(),
                    ),
                  );
                },
                icon: const Icon(Icons.list))
          ],
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'Enter or scan a code:',
                  style: TextStyle(fontSize: 20),
                ),
                const SizedBox(height: 8),
                TextField(
                    controller: _codeInputController,
                    onSubmitted: (value) => _openCode(data, value.toString()),
                    decoration: InputDecoration(
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.open_in_new),
                          color: Theme.of(context).colorScheme.secondary,
                          onPressed: data.loaded
                              ? () => _openCode(data, _codeInputController.text)
                              : null,
                        ),
                        hintText: 'Inventory code',
                        border: const OutlineInputBorder())),
                const SizedBox(height: 8),
                if (nfcAvailable) const Text('NFC is available')
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _scanCode(data),
          tooltip: 'Scan a QR or barcode',
          child: const Icon(Icons.qr_code),
        ),
      );
    });
  }
}
