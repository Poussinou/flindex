import 'package:flindex/flindex_data.dart';
import 'package:flutter/material.dart';
import 'package:flindex/screens/home.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) {
      var d = FlindexData();
      d.loadData();
      return d;
    },
    child: const FlindexApp(),
  ));
}

class FlindexApp extends StatelessWidget {
  const FlindexApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flindex',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const FlindexHome(),
    );
  }
}
