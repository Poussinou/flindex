# Flindex: find stuff again

Flindex is an app to manage the contents of your home.

It uses NFC or QR code stickers to manage the inventory of your boxes and drawers.

Flindex can scan NFC tags or QR codes and show you an associated inventory or let you create a new one.

## Screens
### Home
- The most common usage will be lookup, so the home screen allows the user to scan or manually enter codes.
  - Scanning/entering a known code displays the `Inventory display` associated with that code
  - Scanning/entering an unknown code displays the `Inventory edit` screen for the code with an empty inventory
  - Access to the list overview and search function

### Inventory display
- A list view of the inventory associated with a code.
  - The view is non-editable, a toolbutton displays the inventory edit screen, populated for the currently shown code

### Inventory edit
- Allows the user to enter a `name` for the code (e.g. `top-drawer` or `big transparent box`)
  - Names need not be unique, codes do
  - Saving the name needs to be confirmed by pressing the save button
- Allows the user to manage a list of strings for the items in the inventory
- All items are saved immediately

### Inventory overview
- A list of all registered inventories is shown
  - Selecting an inventory displays it

### Search function
- Entering a search string will bring up all inventories and items matching the string
  - Searching ignores letter case, so `USB` will match `USB` and `usb`

## TODO
- Add nested inventories, e.g. for boxes in a cupboard
